(function (){
	var dave = document.getElementsByTagName('h1')[0],		
		song = document.getElementById('song'),
		square = document.getElementById('square'),

		changeDave = function() {
			if (dave.innerHTML === 'Rush fonts') {
				dave.innerHTML = 'Dave Rivas';
				dave.className = 'dave-change';				
			} else {
				dave.className = 'rush';
				dave.innerHTML = 'Rush fonts';				
			}
		},

		changeSong = function () {
			song.innerHTML = 'Driven <em>up</em> and <em><u>down</u></em> in <span id="red">circles<span>';
		},
		
		returnSong = function() {
			song.innerHTML = 'Mould a new <em>reality,</em> closer to the <u>heart</u>'; 
		};

	dave.addEventListener("click", changeDave);
	square.addEventListener("mouseover", changeSong);
	square.addEventListener("mouseleave", returnSong);
})();